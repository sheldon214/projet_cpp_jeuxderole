#ifndef GUERRIER_H_INCLUDED
#define GUERRIER_H_INCLUDED

#include "personnage.h"

class CGuerrier:public CPersonnage{
public :
    CGuerrier(string);
    void afficher();
    void attaquer(shared_ptr<CPersonnage>,int,string);
};




#endif // GUERRIER_H_INCLUDED
