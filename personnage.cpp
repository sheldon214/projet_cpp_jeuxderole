#include "personnage.h"


CPersonnage::CPersonnage(string nom_)
{
    this->nom = nom_;
    this->vie = 50;
    this->xp = 1;
    this->vie_max=100;
    this->niveaux=1;
    this->armes["default"] = 1 ;

}

CPersonnage::CPersonnage(CPersonnage& Perso)
{
    this->nom = Perso.nom;
    this->vie = Perso.vie;
    this->xp = Perso.xp;
    this->niveaux = Perso.niveaux;
    this->vie_max = Perso.vie_max;

}


string CPersonnage::get_nom(){

return this->nom;
}

int CPersonnage::get_vie()
{
    return vie;
}

int CPersonnage::get_xp()
{
    return xp;
}

int CPersonnage::get_niveau()
{
    return niveaux;
}

int CPersonnage::get_vie_max()
{
    return vie_max;
}

void CPersonnage::set_vie(int Vie_)
{
    this->vie = Vie_>0?Vie_:0;
}
void CPersonnage::set_niveau(int nv)
{
    this->niveaux = nv;
}

void CPersonnage::set_xp(int xp_)
{
    this->xp = xp_;
}

void CPersonnage::set_vie_max(int vie_max_)
{
    this->vie_max = vie_max_;
}


map<string,int> CPersonnage::get_armes(){

    return armes;

}

void CPersonnage::add_armes(string nom_arme,int degat){

    this->armes.insert({nom_arme,degat});
}




void CPersonnage::afficher()
{

}


