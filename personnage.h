#ifndef CLASSES_H_INCLUDED
#define CLASSES_H_INCLUDED

#include <iostream>
#include <string>
#include <memory>
#include <map>

using namespace std;


class CPersonnage{

public :
    CPersonnage(string );

    int get_vie();
    int get_xp();
    int get_niveau();
    int get_vie_max();
    map<string,int> get_armes();

    string get_nom();

    void set_vie(int);
    void set_niveau(int);
    void set_xp(int);
    void set_vie_max(int);
    void add_armes(string,int);

    virtual void afficher();
    virtual void soigner(shared_ptr<CPersonnage>){};
    virtual void attaquer(shared_ptr<CPersonnage>,int,string ){};
    virtual ~CPersonnage(){};



    CPersonnage(CPersonnage&);




protected :
    int vie_max;
    int vie;
    int xp;
    int niveaux;
    string nom;
    map<string,int> armes ;
};


#endif // CLASSES_H_INCLUDED
