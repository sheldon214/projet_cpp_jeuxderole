


#include "saveperso.h"



void afficher(CPersonnage& perso){

    perso.afficher();

}


int main()
{
//    CPersonnage Perso1("Perso1");
//    CMagicien Mage1("mage1");
//    CGuerrier Guer1("Guer1");
//    Mage1.afficher();
//    Guer1.afficher();



    vector<shared_ptr<CPersonnage>> Collection;
    map<string,vector<int>> Index_perso;

    int idx(0);
    int count_(0);
    bool stop(false);
    int choix(0);
    string nom;
    int xp(0);
    int niveau(0);
    int vie(0);
    int vie_max(0);
    string nom_arme("default");
    int degat_arme(1);


    count_ = loadperso("perso.txt",Index_perso,Collection);

    while(!stop) {

        cout << "Menu : \n"
             << "\t1.creer un perso" << endl
             << "\t2.Modifier Perso" << endl
             << "\t3.afficher les perso" << endl
             << "\t4.Soigner"   << endl
             << "\t5.Nombre de perso" << endl
             << "\t6.attaquer"  << endl
             << "\t7.charger" << endl
             << "\t8.sauvegarder" << endl
             << "\t9.quitter " << endl;

        cout << ">" ;cin >> choix;
        switch(choix){

                case 1 :
                    {
                        cout << "type de Perso : \n 1.Mage \t 2.Guerrier \t 3.Soigneur " <<endl;
                        cout << ">" ;cin >> choix;
                        cout << "Nom du Perso : ";
                        cin >> nom;
                        if(choix==1){
//                            CMagicien Mage(nom);

                            Collection.push_back(make_shared <CMagicien> (nom));
                            Index_perso["mage"].push_back(count_);
                            count_++;
                        }
                        else if (choix==2){
//                            CGuerrier Guerr(nom);
                            Collection.push_back(make_shared <CGuerrier> (nom));
                            Index_perso["guerrier"].push_back(count_);
                            count_++;
                        }
                        else if (choix==3){

                            Collection.push_back(make_shared <CSoigneur> (nom));
                            Index_perso["soigneur"].push_back(count_);
                            count_++;
                        }


                    }
                break;

                case 2 :
                    {
                        cout << "Saisir id Perso : ";
                        cin >> idx;

                        if (idx < count_ ){

                            cout << "\nXp : ";
                            cin >> xp;

                            Collection[idx]->set_xp(xp);

                            cout << "\nVie : ";
                            cin >> vie;
                            Collection[idx]->set_vie(vie);

                            cout << "\nNiveau : ";
                            cin >> niveau;
                            Collection[idx]->set_niveau(niveau);

                            cout << "\nVie Max : ";
                            cin >> vie_max;
                            Collection[idx]->set_vie_max(vie_max);

                            cout << "\nNombre d'armes a rajouter : ";

                            int count_arme;
                            cin >> count_arme;
                            for (int i =0;i<count_arme;i++){
                                cout << "arme " << i+1 << " : ";
                                cin >> nom_arme;
                                cout << "degat : " ;
                                cin >> degat_arme  ;
                                Collection[idx]->add_armes(nom_arme,degat_arme);
                            }

                        }
                        else {
                                    cout << "max id perso  : " << count_<< endl;
                            }



                    }
                break;
                case 3 :
                    {

                        int i =0;
                        for (auto perso : Collection){
                            perso->afficher();
                            cout << "\t Nom : " << perso->get_nom() << endl;
                            cout << "\t Id  : " << i << endl;
                            cout << "\t Vie : " << perso->get_vie() << endl;
                            cout << "\t Vie max : " << perso->get_vie_max() << endl;
                            cout << "\t Niveau  : " << perso->get_niveau() << endl;
                            cout << "\t Xp   : " << perso->get_xp() << endl;
                            cout << "\t Armes { ";
                            map<string, int> armes = perso->get_armes();
                            map<string, int>::iterator it;
                            for (it = armes.begin(); it != armes.end(); it++)
                            {
                                std::cout << it->first    // string (key)
                                          << ':'
                                          << it->second   // string's value
                                          << " ";
                            }
                            cout << "}" << endl;

                            i++;
                        }
                    }
                    break;

                case 4 :

                    {
                        cout << "entrer Id du soigneur  : " ;
                        int idx_soigneur(0);
                        cin >> idx_soigneur;
                        if(find(Index_perso["soigneur"].begin(), Index_perso["soigneur"].end(), idx_soigneur) != Index_perso["soigneur"].end()){
                            cout << "entrer Id du Perso a soigner : ";
                            cin >> idx;
                            if (idx < count_ ){
                                    Collection[idx_soigneur]->soigner(Collection[idx]);
                                    }
                            else {
                                    cout << "max id perso  : " << count_ << endl;
                            }
                        }
                        else {
                            cout << "ce soigneur n'existe pas "<< endl;
                        }

                    }
                break;


                case 5 :

                    {
                            cout << "Mage : " << Index_perso["mage"].size() << endl;
                            cout << "Guerrier : " << Index_perso["guerrier"].size() << endl;
                            cout << "Soigneur : " << Index_perso["soigneur"].size() << endl;
                    }
                break;

                case 6 :
                    {
                        int id_attaquant(0);
                        int id_attaquer(0);
                        cout << "ID joueur attaquant : ";
                        cin >> id_attaquant;
                        cout << "ID joueur attaquer : ";
                        cin >> id_attaquer;
                        if(find(Index_perso["soigneur"].begin(), Index_perso["soigneur"].end(), id_attaquant) != Index_perso["soigneur"].end()
                           ||id_attaquant>=count_||id_attaquer >=count_
                           )
                        {
                            cout << "Methode attaquer indisponible pour ce perso[soigneur ou perso inexistant]" << endl;
                        }

                        else{
                        cout << "Arme : ";
                        cin >> nom_arme;
                        cout << "degat : ";
                        cin >> degat_arme;
                        Collection[id_attaquant]->attaquer(Collection[id_attaquer],degat_arme,nom_arme);
                        }
                    }
                break;


                case 7 :
                    {
                        count_ = loadperso("perso.txt",Index_perso,Collection);
                    }

                break;

                case 8 :
                    {
                        saveperso("perso.txt",Index_perso,Collection);
                    }

                break;
                default :
                    {

                        stop = true ;
                        saveperso("perso.txt",Index_perso,Collection);

                    }
                break;

        }







    }



    return 0;
}
