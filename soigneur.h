#ifndef SOIGNEUR_H_INCLUDED
#define SOIGNEUR_H_INCLUDED

#include "personnage.h"

class CSoigneur:public CPersonnage{
public :
    CSoigneur(string);
    void afficher();
    void soigner(shared_ptr<CPersonnage>);
//    void attaquer(shared_ptr<CPersonnage>,int){};
};



#endif // SOIGNEUR_H_INCLUDED
