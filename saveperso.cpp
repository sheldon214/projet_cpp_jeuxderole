#include "saveperso.h"

void saveperso(string pathfile, map<string,vector<int>> Index_perso,vector<shared_ptr<CPersonnage>> Collection){

    ofstream fichier(pathfile);
    cout << "start of saving ..." << endl;

    for(int i =0; i<Collection.size();i++){

        cout << " Saving perso ... " << i+1 << endl;
        if(find(Index_perso["soigneur"].begin(),
                Index_perso["soigneur"].end(),
                i) != Index_perso["soigneur"].end())
        {
            fichier << "soigneur" ;

        }
        else if(find(Index_perso["mage"].begin(),
                Index_perso["mage"].end(),
                i) != Index_perso["mage"].end())
        {
            fichier << "mage" ;

        }

        else if(find(Index_perso["guerrier"].begin(),
                Index_perso["guerrier"].end(),
                i) != Index_perso["guerrier"].end())
        {
            fichier << "guerrier" ;
        }

        fichier << " " << Collection[i]->get_nom()
                << " " << Collection[i]->get_vie()
                << " " << Collection[i]->get_vie_max()
                << " " << Collection[i]->get_niveau()
                << " " << Collection[i]->get_xp();

        map<string, int> armes = Collection[i]->get_armes();
        map<string, int>::iterator it;
        fichier << " ";
        for (it = armes.begin(); it != armes.end(); it++)
        {
            fichier   << it->first    // string (key)
                      << " "
                      << it->second   // string's value
                      << " ";
        }
        fichier << endl;

    }

    cout << "end of saving ..." << endl;


}


int loadperso(string pathfile, map<string,vector<int>>& Index_perso,vector<shared_ptr<CPersonnage>>& Collection){

    ifstream fichier(pathfile);
    cout << "start of loading ..." << endl;

    int i = 0;

    string line {""};

    // boucle qui lit toute les lignes
    while(getline(fichier,line))
          {
        cout << " loading perso ... " << i+1 << endl;
        stringstream streamline(line);  // convertir le string line en stream


        shared_ptr <CPersonnage> perso; // initialisation d'un pointeur sur un perso
        //boucle qui lit les attribut d'un personnage
        string typeperso("");
        string mot("");

        getline(streamline,typeperso,' ');

        if(typeperso=="soigneur")
        {
            getline(streamline,mot,' ');
           perso = make_shared <CSoigneur>(mot);
           Index_perso["soigneur"].push_back(i);

        }
        else if(typeperso=="mage")
        {
            getline(streamline,mot,' ');
            perso = make_shared <CMagicien>(mot);
            Index_perso["mage"].push_back(i);

        }
        else if(typeperso=="guerrier")
        {
             getline(streamline,mot,' ');
            perso = make_shared <CGuerrier>(mot);
            Index_perso["guerrier"].push_back(i);

        }



        getline(streamline,mot,' ');
        const string mot_vie(mot);
        int nbre = stoi(mot_vie);
        perso->set_vie(nbre);

        getline(streamline,mot,' ');
        const string mot_vie_max(mot);
        nbre = stoi(mot_vie_max);
        perso->set_vie_max(nbre);

        getline(streamline,mot,' ');
        const string mot_niveau(mot);
        nbre = stoi(mot_niveau);
        perso->set_niveau(nbre);

        getline(streamline,mot,' ');
        const string mot_xp(mot);
        nbre = stoi(mot_xp);
        perso->set_xp(nbre);


        string nomarme("");
        while(getline(streamline,nomarme,' ')){

            getline(streamline,mot,' ');
            const string mot_arme(mot);
            nbre = stoi(mot_arme);
            perso->add_armes(nomarme,nbre);
        }




        Collection.push_back(perso);
        i++;

    }

    cout << "end of loading ..." << endl;

    return i;

}

