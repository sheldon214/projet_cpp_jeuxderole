
#include "magicien.h"

CMagicien::CMagicien(string nom) : CPersonnage(nom)
{

}

void CMagicien::afficher(){

    cout << "[magicien]" << endl;

}

void CMagicien::attaquer(shared_ptr<CPersonnage> perso,int degat ,string arme){

    cout << "attaque de Mage -> " << arme << endl;
    perso->set_vie(perso->get_vie() - degat*this->armes[arme]);

}


