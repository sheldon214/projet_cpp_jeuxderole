#ifndef MAGICIEN_H_INCLUDED
#define MAGICIEN_H_INCLUDED

#include "personnage.h"




class CMagicien:public CPersonnage{
public :
    CMagicien(string );
    void afficher();
    void attaquer(shared_ptr<CPersonnage>,int,string);



};




#endif // MAGICIEN_H_INCLUDED
