
#include "guerrier.h"

CGuerrier::CGuerrier(string nom):CPersonnage(nom)
{

}

void CGuerrier::afficher()
{

    cout << "[guerrier]" << endl;

}

void CGuerrier::attaquer(shared_ptr<CPersonnage> perso,int degat ,string arme){

    cout << "attaque de Guerrier -> " << arme << endl;
    perso->set_vie(perso->get_vie() - degat*this->armes[arme]);
}
