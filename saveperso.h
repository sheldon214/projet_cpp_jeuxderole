#ifndef SAVEPERSO_H_INCLUDED
#define SAVEPERSO_H_INCLUDED

#include <iostream>
#include<fstream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <memory>
#include <sstream>
#include "Personnage.h"
#include "magicien.h"
#include "guerrier.h"
#include "soigneur.h"

using namespace std;

void saveperso(string , map<string,vector<int>> ,vector<shared_ptr<CPersonnage>>);
int loadperso(string , map<string,vector<int>>& ,vector<shared_ptr<CPersonnage>>& );

#endif // SAVEPERSO_H_INCLUDED
